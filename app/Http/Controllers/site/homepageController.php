<?php

namespace App\Http\Controllers\site;


use App\Product;
use App\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class homepageController extends Controller
{
    public function index()
    {
        if (cache('allProducts')){
            $allProducts = cache('allProducts');
        } else{
            $allProducts = Product::latest()->take(8)->get();
            cache(['allProducts' => $allProducts] , Carbon::now()->addMinute(5));
        }
        return view('site.homePage.homePage', compact('allProducts'));
    }
}
