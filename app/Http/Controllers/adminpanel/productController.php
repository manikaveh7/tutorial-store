<?php

namespace App\Http\Controllers\adminpanel;

use App\Product;
use App\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hameye_mahsolat = Product::all();
//        dd($hameye_mahsolat);
        return view('adminpanel.product.index', compact('hameye_mahsolat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategories = ProductCategory::all();
        return view('adminpanel.product.create' , compact('allCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => ['required' , 'min:3' , 'max:255'],
                'category' => ['required'],
                'price' => ['required'],
                'product_id'=> ['required'],
                'image' => ['required'],
                'status' => ['required']
            ],
            [
                'title.required' => 'عنوان محصول الزامی است.',
                'category.required' => 'دسته بندی محصول الزامی است.',
                'title.min' => 'عنوان محصول نمی تواند کمتر از 3 کاراکتر باشد.',
                'title.max' => 'عنوان محصول نمی تواند بیشتر از 255 کاراکتر باشد.',
            ]);


        if ($request->hasFile('image')) {
            $image = '';

            $destination = public_path() . config('cms-setting.url');
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $file->move($destination, $filename);
//            $image = config('cms-setting.url') . $filename;
            $image = config('cms-setting.url') . '/' . $filename;
            $thumbnail = $image;

            Product::create([
                'title' => $request->get('title'),
//                'slug' => $request->get('title'),
                'description' => $request->get('description'),
                'weight' => $request->get('weight'),
                'size' => $request->get('size'),
                'count' => $request->get('count'),
                'price' => $request->get('price'),
                'old_price' => $request->get('old_price'),
                'thumbnail' => $thumbnail,
                'image' => $image,
                'category' => $request->get('category'),
                'status' => $request->get('status'),
                'discount' => $request->get('discount'),
                'product_id' => $request->get('product_id'),
                'rate'=> 1,
            ]);

            return redirect(route('dashboard.product.index'))->with('message' , 'محصول شما با موفقیت در سایت ثبت شد.');
        }
        else{
            return redirect(route('dashboard.product.index'))->with('error' , 'مشکلی در ثبت محصول وجود دارد.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $allCategories = ProductCategory::all();
        return view('adminpanel.product.edit' , compact( ['product' , 'allCategories']  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => ['required' , 'min:3' , 'max:255'],
            'category' => ['required'],
            'price' => ['required'],
            'status' => ['required']
        ],
        [
            'title.required' => 'عنوان محصول الزامی است.',
            'category.required' => 'دسته بندی محصول الزامی است.',
            'title.min' => 'عنوان محصول نمی تواند کمتر از 3 کاراکتر باشد.',
            'title.max' => 'عنوان محصول نمی تواند بیشتر از 255 کاراکتر باشد.',
        ]);


        $product = Product::findOrFail($id);


        if ($request->hasFile('image')){
            $destination = public_path() . config('cms-setting.url');
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $file->move($destination, $filename);
            $image = config('cms-setting.url') . '/' . $filename;
            $thumbnail = $image;

        }
        else{
            $image = $product->image;
            $thumbnail = $image;
        }

        $product->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'weight' => $request->get('weight'),
            'size' => $request->get('size'),
            'count' => $request->get('count'),
            'price' => $request->get('price'),
            'old_price' => $request->get('old_price'),
            'thumbnail' => $thumbnail,
            'image' => $image,
            'category' => $request->get('category'),
            'status' => $request->get('status'),
            'discount' => $request->get('discount'),
            'product_id' => $product->product_id,
        ]);
        $product->save();
        $mess = 'محصول ' . $product->title . ' با موفقیت ویرایش شد.';
        return redirect()->route('dashboard.product.index')->with('message' , $mess);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if (!$product){
            return redirect()->route('dashboard.product.index')->with('error' , 'محصول مورد نظر موجود نمی باشد.');
        }
        else{
            $mess = 'محصول ' . $product->title . ' با موفقیت پاک شد.';
            $product->delete();
            return redirect()->route('dashboard.product.index')->with('warning' , $mess);
        }
    }

    public function uploadImage()
    {
        $this->validate(request(),[
           'upload' => 'required'
        ]);
        $image = '';

        $imagePath = "/upload/images/2020/";
        $file = request()->file('upload');
        $filename = $file->getClientOriginalName();
        if(file_exists(public_path($imagePath) . $filename)) {
            $filename = Carbon::now()->timestamp . $filename;
        }

        $file->move(public_path($imagePath) , $filename);
        $url = $imagePath . $filename;

        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , '')</script>";
    }
}
