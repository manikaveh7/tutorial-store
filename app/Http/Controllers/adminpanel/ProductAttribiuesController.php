<?php

namespace App\Http\Controllers\adminpanel;

use App\Product;
use App\ProductAttribiutes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductAttribiuesController extends Controller
{
    public function index()
    {
        $allProductAttr = ProductAttribiutes::all();
        return view('adminpanel.productAttr.index' , compact('allProductAttr'));
    }

    public function create()
    {
        $allProdcuts = Product::all();
        return view('adminpanel.productAttr.create' , compact('allProdcuts'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'key' => 'required',
            'value' => 'required',
        ] , [
            'product_id.required' => 'عنوان ویژگی الزامی است.',
            'key.required' => 'عنوان ویژگی الزامی است.',
            'value.required' => 'عنوان ویژگی الزامی است.',
        ]);

        ProductAttribiutes::create([
           'product_id' => $request->get('product_id'),
           'key' => $request->get('key'),
           'value' => $request->get('value'),
        ]);

        return redirect()->route('dashboard.productAttr.index')->with('message' , 'ویژگی جدید با موفقیت در سایت ثبت شد');
    }

    public function edit($id)
    {
        $allProdcuts = Product::all();
        $PrAtt = ProductAttribiutes::findOrFail($id);
        return view('adminpanel.productAttr.edit' , compact(['allProdcuts' , 'PrAtt']));
    }
}
