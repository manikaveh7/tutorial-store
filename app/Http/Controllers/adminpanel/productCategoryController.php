<?php

namespace App\Http\Controllers\adminpanel;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class productCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCategory = ProductCategory::all();
        return view('adminpanel.productCategory.index', compact('allCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategories = ProductCategory::all();
        return view('adminpanel.productCategory.create' , compact('allCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = '';
        if ($request->hasFile('image')){
            $destination = public_path() . config('cms-setting.url_product_category');
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $file->move($destination, $filename);
            $image = config('cms-setting.url_product_category') . '/' . $filename;
        }
        else{
            $image = '';
        }
        $description = '';
        if ($request->get('description') == null || $request->get('description') == ''){
            $description = '';
        }
        else{
            $description = $request->get('description');
        }

        ProductCategory::create([
            'topic' => $request->get('topic'),
            'description'   => $description,
            'image' => $image,
            'status' => $request->get('status'),
            'parent_id' => $request->get('parent_id'),
        ]);

        return redirect()->route('dashboard.productCategory.index')->with('message','دسته بندی جدید با موفقیت ثبت شد.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorySelected = ProductCategory::findOrFail($id);
        $allCategories = ProductCategory::all();
        return view('adminpanel.productCategory.edit', compact(['categorySelected', 'allCategories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = ProductCategory::findOrFail($id);
        if ($request->hasFile('image')){
            $destination = public_path() . config('cms-setting.url_product_category');
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $file->move($destination, $filename);
            $image = config('cms-setting.url_product_category') . '/' . $filename;
            $thumbnail = $image;

        }
        else{
            $image = $category->image;
            $thumbnail = $image;
        }
        $category->update([
            'topic' => $request->get('topic'),
            'description'   => $request->get('description'),
            'image' => $image,
            'status' => $request->get('status'),
            'parent_id' => $request->get('parent_id'),
        ]);

        $category->save();
        $mess = 'دسته بندی ' . $category->topic . ' با موفقیت ویرایش شد.';
        return redirect()->route('dashboard.productCategory.index')->with('message', $mess);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ProductCategory::findOrFail($id);
        if (!$category){
            return redirect()->route('dashboard.productCategory.index')->with('message', 'دسته بندی مورد نظر در سایت موجود نمی باشد.');
        }
        else{
            $mess = 'دسته بندی ' . $category->topic . ' با موفقیت از سایت حذف شد.';
            $category->delete();
            return redirect()->route('dashboard.productCategory.index')->with('warning', $mess);
        }
    }
}
