<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Session;
use Melipayamak\MelipayamakApi;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $data['mobile'] = $request->get('mobile');
        $data['verify'] = $request->get('password');

        $user = User::where('mobile' , $data['mobile'])->first();
        if (!$user){
            return redirect(route('login'))->with('error','یوزر مورد نظر موجود نمی باشد.');
        }
        elseif ($user->verify_code != $data['verify']){
            return redirect(route('login'))->with('error','رمز عبور صحیح نمی باشد.');
        }
        elseif ($user->role == 'user'){
            Auth::login($user);
            return redirect(route('user.account.index'));
        }
        Auth::login($user);
        return redirect(route('dashboard'));
    }

    public function sendCode(Request $request)
    {
        $mobile = $request->input('mobile_num');
        $user = User::where('mobile' , $mobile)->first();

        if (!$user){
            return "fail";
        }
        else{
            $verify_code_make = rand(10000,99999);

            $user->update([
                'verify_code' => $verify_code_make
            ]);

            $api = new MelipayamakApi('09378248297', '9279');
            $sms = $api->sms();
            $to = $user->mobile;
            $from = config('50004000970097');
            $text = 'رمز ورود برای فروشگاه آموزشی ' . $verify_code_make;
            $sms->send($to,$from,$text);
            return $verify_code_make;
        }
    }

    public function logout()
    {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect()->route('login');
    }
}
