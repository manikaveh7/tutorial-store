<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttribiutes extends Model
{
    protected $fillable = [
      'product_id' , 'key' , 'value'
    ];

    public function getProductName($id)
    {
        $thisProduct = Product::where('id' , '=' , $id)->first();
        return $thisProduct->title;
    }
}
