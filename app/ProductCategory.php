<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductCategory extends Model
{
    use Sluggable;

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    protected $table = 'productcategory';

    protected $fillable = [
        'topic', 'description' , 'parent_id' , 'image' , 'status'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'topic'
            ]
        ];
    }

    public function parent_name($id)
    {
        $name_parent = ProductCategory::where('id' , $id)->first();

        if (!$name_parent){
            return 'دسته بندی مادر';
        }
        else{
            $name_parent = $name_parent->topic;
            return $name_parent;
        }
    }

    public function countsProducts($id)
    {
        $all = Product::where('category' , '=' , $id)->all();
        return $all->count();
    }

    public function CountsOfProducts($id){
        $numberOfProducts = Product::all()->where('category' , '=' , $id);

        return $numberOfProducts->count();
    }

}
