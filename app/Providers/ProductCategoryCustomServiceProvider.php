<?php

namespace App\Providers;

use App\ProductCategory;
use Illuminate\Support\ServiceProvider;

class ProductCategoryCustomServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('site.layout.header.header-bottom' , function($view){
            $allCategories = ProductCategory::all();
            return $view->with('allCategories' , $allCategories);
        });

        view()->composer('site.shop.index' , function($view){
            $allCategories = ProductCategory::all();
            return $view->with('allCategories' , $allCategories);
        });
    }
}
