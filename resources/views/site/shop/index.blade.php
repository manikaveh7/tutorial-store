@extends('site.layout.inc.main')

@section('page-title')
    فروشگاه اصلی
@stop

@section('main-content')
    <div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title">فروشگاه<span>فروشگاه</span></h1>
        </div><!-- End .container -->
    </div>
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">خانه</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('site.shop.index') }}">فروشگاه</a></li>
            </ol>
        </div><!-- End .container -->
    </nav>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    @if($allProductsCount > 0)
                        <div class="toolbox">
                            <div class="toolbox-left">
                                <div class="toolbox-info">
                                    تعداد
                                    <span>
                                        {{ $allProductsCount }}
                                    </span>
                                    محصول
                                </div><!-- End .toolbox-info -->
                            </div><!-- End .toolbox-left -->

                            <div class="toolbox-right">
                                <div class="toolbox-sort">
                                    <label for="sortby">مرتب سازی براساس : </label>
                                    <div class="select-custom">
                                        <select name="sortby" id="sortby" class="form-control">
                                            <option value="popularity" selected="selected">بیشترین خرید</option>
                                            <option value="rating">بیشترین امتیاز</option>
                                            <option value="date">تاریخ</option>
                                        </select>
                                    </div>
                                </div><!-- End .toolbox-sort -->
                                <div class="toolbox-layout">
                                    <a href="category-list.html" class="btn-layout">
                                        <svg width="16" height="10">
                                            <rect x="0" y="0" width="4" height="4"></rect>
                                            <rect x="6" y="0" width="10" height="4"></rect>
                                            <rect x="0" y="6" width="4" height="4"></rect>
                                            <rect x="6" y="6" width="10" height="4"></rect>
                                        </svg>
                                    </a>

                                    <a href="category-2cols.html" class="btn-layout">
                                        <svg width="10" height="10">
                                            <rect x="0" y="0" width="4" height="4"></rect>
                                            <rect x="6" y="0" width="4" height="4"></rect>
                                            <rect x="0" y="6" width="4" height="4"></rect>
                                            <rect x="6" y="6" width="4" height="4"></rect>
                                        </svg>
                                    </a>

                                    <a href="category.html" class="btn-layout">
                                        <svg width="16" height="10">
                                            <rect x="0" y="0" width="4" height="4"></rect>
                                            <rect x="6" y="0" width="4" height="4"></rect>
                                            <rect x="12" y="0" width="4" height="4"></rect>
                                            <rect x="0" y="6" width="4" height="4"></rect>
                                            <rect x="6" y="6" width="4" height="4"></rect>
                                            <rect x="12" y="6" width="4" height="4"></rect>
                                        </svg>
                                    </a>

                                    <a href="category-4cols.html" class="btn-layout active">
                                        <svg width="22" height="10">
                                            <rect x="0" y="0" width="4" height="4"></rect>
                                            <rect x="6" y="0" width="4" height="4"></rect>
                                            <rect x="12" y="0" width="4" height="4"></rect>
                                            <rect x="18" y="0" width="4" height="4"></rect>
                                            <rect x="0" y="6" width="4" height="4"></rect>
                                            <rect x="6" y="6" width="4" height="4"></rect>
                                            <rect x="12" y="6" width="4" height="4"></rect>
                                            <rect x="18" y="6" width="4" height="4"></rect>
                                        </svg>
                                    </a>
                                </div><!-- End .toolbox-layout -->
                            </div><!-- End .toolbox-right -->
                        </div><!-- End .toolbox -->

                        <div class="products mb-3">
                            <div class="row justify-content-center">
                                @foreach($allProducts as $item)
                                    <div class="col-6 col-md-4 col-lg-4 col-xl-3">
                                        <div class="product product-7 text-center">
                                            <figure class="product-media">
                                                <span class="product-label label-new">جدید</span>
                                                <a href="{{ route('site.product.index' , $item->id) }}">
                                                    <img src="{{ url('') }}{{ $item->image }}" alt="{{ $item->title }}" class="product-image">
                                                </a>
                                                <div class="product-action">
                                                    <a href="#" class="btn-product btn-cart"><span>افزودن به سبد خرید</span></a>
                                                </div><!-- End .product-action -->
                                            </figure><!-- End .product-media -->

                                            <div class="product-body">
                                                <div class="product-cat text-center">
                                                    <a href="{{ route('site.productCategory.index' , $item->category) }}">{{ $item->category_name($item->id) }}</a>
                                                </div><!-- End .product-cat -->
                                                <h3 class="product-title text-center">
                                                    <a href="{{ route('site.product.index' , $item->id) }}">{{ $item->title }}</a>
                                                </h3><!-- End .product-title -->
                                                <div class="product-price">
                                                    {{ number_format($item->price) }} تومان
                                                </div><!-- End .product-price -->
                                                <div class="ratings-container">
                                                    {{--                                            <span class="ratings-text">0</span>--}}
                                                    <div class="ratings">
                                                        @switch($item->rate)
                                                            @case(1)
                                                            <div class="ratings-val" style="width: 20%;"></div>
                                                            @break
                                                            @case(2)
                                                            <div class="ratings-val" style="width: 40%;"></div>
                                                            @break
                                                            @case(3)
                                                            <div class="ratings-val" style="width: 60%;"></div>
                                                            @break
                                                            @case(4)
                                                            <div class="ratings-val" style="width: 80%;"></div>
                                                            @break
                                                            @case(5)
                                                            <div class="ratings-val" style="width: 100%;"></div>
                                                        @break
                                                    @endswitch
                                                    <!-- End .ratings-val -->
                                                    </div><!-- End .ratings -->
                                                </div><!-- End .rating-container -->
                                            </div><!-- End .product-body -->
                                        </div><!-- End .product -->
                                    </div><!-- End .col-sm-6 col-lg-4 col-xl-3 -->
                                @endforeach
                            <div class="col-sm-12">
                                {{ $allProducts->links() }}
{{--                                {!! $allProducts->render() !!}--}}
                            </div>
                            </div><!-- End .row -->
                        </div><!-- End .products -->
                    @else
                        <p class="text-danger">هیچ محصولی درسایت موجود نمی باشد. </p>
                    @endif
                </div><!-- End .col-lg-9 -->
                <aside class="col-lg-3 order-lg-first">
                    <div class="sidebar sidebar-shop">
                        <div class="widget widget-clean">
                            <label>فیلترها : </label>
                            {{--                            <a href="#" class="sidebar-filter-clear">پاک کردن همه</a>--}}
                        </div><!-- End .widget widget-clean -->

                        <div class="widget widget-collapsible">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-1" role="button" aria-expanded="true" aria-controls="widget-1">
                                    همه دسته بندی ها
                                </a>
                            </h3><!-- End .widget-title -->

                            <div class="collapse show" id="widget-1">
                                <div class="widget-body">
                                    <div class="filter-items filter-items-count">
                                        @foreach($allCategories as $category)
                                            <div class="filter-item">
                                                <div class="custom-control custom-checkbox">
                                                    <a href="{{ route('site.productCategory.index' , $category->id) }}" class="">{{ $category->topic }}</a>
                                                </div><!-- End .custom-checkbox -->
                                                <span class="item-count">{{ $category->CountsOfProducts($category->id) }}</span>
                                            </div><!-- End .filter-item -->
                                        @endforeach
                                    </div><!-- End .filter-items -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->

                    </div><!-- End .sidebar sidebar-shop -->
                </aside><!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div>
@stop
