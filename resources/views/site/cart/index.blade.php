@extends('site.layout.inc.main')

@section('page-title')
    سبد خرید
@stop

@section('main-content')

    <div class="page-header text-center" style="background-image: url('assets/images/page-header-bg.jpg')">
        <div class="container">
            <h1 class="page-title">سبد خرید<span>فروشگاه</span></h1>
        </div><!-- End .container -->
    </div>
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">خانه</a></li>
                <li class="breadcrumb-item"><a href="#">فروشگاه</a></li>
                <li class="breadcrumb-item active" aria-current="page">سبد خرید</li>
            </ol>
        </div><!-- End .container -->
    </nav>
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <div class="row">
                    @if(Cart::content()->count() == 0)
                    <div class="col-sm-12">
                        <div class="alert alert-danger col-sm-9 text-right">
                            <li style="list-style: none">سبد خرید شما خالی است!</li>
                        </div>
                    </div>
                    @else
                    <div class="col-lg-9">
                        @if(session('success_message'))
                            <div class="alert alert-success col-sm-9 text-right">
                                <li style="list-style: none">{{ Session::get('success_message') }}</li>
                            </div>
                        @endif

                        @if(session('error'))
                            <div class="alert alert-danger col-sm-9 text-right">
                                <li style="list-style: none">{{ Session::get('error') }}</li>
                            </div>
                        @endif

                        <table class="table table-cart table-mobile">
                            <thead>
                            <tr>
                                <th>محصول</th>
                                <th>قیمت</th>
                                <th>تعداد</th>
                                <th>مجموع</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach(Cart::content() as $item)
                                <tr>
                                    <td class="product-col">
                                        <div class="product">
                                            <figure class="product-media">
                                                <a href="{{ route('site.product.index' , $item->model->id) }}">
                                                    <img src="{{ url('') }}{{ $item->model->image }}" alt="تصویر محصول">
                                                </a>
                                            </figure>

                                            <h3 class="product-title">
                                                <a href="{{ route('site.product.index' , $item->model->id) }}">{{ $item->model->title }}</a>
                                            </h3><!-- End .product-title -->
                                        </div><!-- End .product -->
                                    </td>
                                    <td class="price-col">{{ number_format($item->model->price) }} تومان</td>
                                    <td class="quantity-col">
                                        <div class="cart-product-quantity">
                                            <input type="hidden" class="form-control" style="display: none;">
                                            <div class="input-group  input-spinner">
                                                <div class="input-group-prepend">
                                                    @if($item->qty <= 1)
                                                        <form action="{{ route('site.cart.destroy' , $item->rowId) }}" method="POST">
                                                            @csrf
                                                            {{ method_field('DELETE') }}
                                                            <button type="submit" class="btn btn-decrement btn-spinner"><i class="icon-close"></i></button>
                                                        </form>
                                                    @else
                                                        <form action="{{ route('site.cart.update.d') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="newQty" id="qty" value="{{ $item->qty }}">
                                                            <input type="hidden" name="rowId" id="rowId" value="{{ $item->rowId }}">
                                                            <input type="hidden" name="productCount" id="rowId" value="{{ $item->model->count }}">
                                                            <button style="min-width: 26px" class="btn btn-decrement btn-spinner" type="submit"><i class="icon-minus"></i></button>
                                                        </form>
                                                    @endif
                                                </div>
                                                <input type="text" id="upCart{{$item->id}}" value="{{ $item->qty }}" min="1" max="{{ $item->count ? $item->count : 10 }}" step="1" data-decimals="0" required=""  style="text-align: center" class="form-control " >

                                                <div class="input-group-append">
                                                    <form action="{{ route('site.cart.update.i') }}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="newQty" id="qty" value="{{ $item->qty }}">
                                                        <input type="hidden" name="rowId" id="rowId" value="{{ $item->rowId }}">
                                                        <input type="hidden" name="productCount" id="rowId" value="{{ $item->model->count }}">
                                                        <button style="min-width: 26px" class="btn btn-increment btn-spinner" type="submit"><i class="icon-plus"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- End .cart-product-quantity -->
                                    </td>
                                    <td class="total-col">{{ number_format($item->model->price * $item->qty) }} تومان</td>

                                    <td class="remove-col">
                                        <form action="{{ route('site.cart.destroy' , $item->rowId) }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn-remove"><i class="icon-close"></i></button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table><!-- End .table table-wishlist -->

{{--                        <div class="cart-bottom">--}}
{{--                            <div class="cart-discount">--}}
{{--                                <form action="#">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input type="text" class="form-control" required="" placeholder="کد تخفیف">--}}
{{--                                        <div class="input-group-append">--}}
{{--                                            <button class="btn btn-outline-primary-2" type="submit"><i class="icon-long-arrow-left mr-0"></i></button>--}}
{{--                                        </div><!-- .End .input-group-append -->--}}
{{--                                    </div><!-- End .input-group -->--}}
{{--                                </form>--}}
{{--                            </div><!-- End .cart-discount -->--}}

{{--                            <a href="#" class="btn btn-outline-dark-2"><span>به روز رسانی سبد خرید</span><i class="icon-refresh"></i></a>--}}
{{--                        </div><!-- End .cart-bottom -->--}}
                    </div><!-- End .col-lg-9 -->
                    <aside class="col-lg-3">
                        <div class="summary summary-cart">
                            <h3 class="summary-title">جمع سبد خرید</h3><!-- End .summary-title -->

                            <table class="table table-summary">
                                <tbody>
                                <tr class="summary-subtotal">
                                    <td>جمع کل سبد خرید : </td>
                                    <td class="text-left">{{ Cart::subtotal() }} تومان</td>
                                </tr><!-- End .summary-subtotal -->
                                <tr class="summary-subtotal">
                                    <td>مالیات (9درصد) : </td>
                                    <td class="text-left">{{ Cart::tax() }} تومان</td>
                                </tr><!-- End .summary-subtotal -->

                                <tr class="summary-total">
                                    <td>مبلغ قابل پرداخت :</td>
                                    <td class="text-left">{{ Cart::total() }} تومان</td>
                                </tr><!-- End .summary-total -->
                                </tbody>
                            </table><!-- End .table table-summary -->

                            <a href="checkout.html" class="btn btn-outline-primary-2 btn-order btn-block">رفتن
                                به صفحه پرداخت</a>
                        </div><!-- End .summary -->

                        <a href="category.html" class="btn btn-outline-dark-2 btn-block mb-3"><span>ادامه
                                        خرید</span><i class="icon-refresh"></i></a>
                    </aside><!-- End .col-lg-3 -->
                    @endif
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .cart -->
    </div>

@stop
