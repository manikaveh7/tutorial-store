@extends('adminpanel.layout')

@section('pageTitle')
    دسته بندی محصولات
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">همه ویژگی محصولات</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">ویژگی محصولات</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('message'))
                        <div class="alert alert-success col-sm-3">
                            <li>{{ Session::get('message') }}</li>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-success col-sm-3">
                            <li>{{ Session::get('error') }}</li>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">جدول ویژگی ها</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(\App\ProductAttribiutes::all()->count() > 0)
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام محصول</th>
                                        <th>عنوان ویژگی</th>
                                        <th>مقدار ویژگی</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allProductAttr as $attr)
                                        <tr>
                                            <td>{{ $attr->id }}</td>
                                            <td>{{ $attr->getProductName($attr->product_id) }}</td>
                                            <td>{{ $attr->key }}</td>
                                            <td>{{ $attr->value }}</td>
                                            <td>
                                                <a href="{{ route('dashboard.productAttr.edit' , $attr->id) }}" style="color: gray"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="{{ route('dashboard.productAttr.destroy' , $attr->id) }}" style="color: red"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                            @else
                                <h4>ویژگی محصولی در سایت موجود نیست</h4>
                                <a class="btn btn-outline-success" href="{{ route('dashboard.productAttr.create') }}">اولین ویژگی خود را بساز</a>
                            @endif
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop


@section('footerScripts')

    <!-- DataTables -->
    <script src="{{ url('adminPanel/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#categories').addClass('menu-open');
        $('#categories > a').addClass('active');
        $('#allCategories').addClass('active');

    </script>
@stop

