@extends('adminpanel.layout')

@section('pageTitle')
    افزودن ویژگی جدید
@stop


@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">اافزودن ویژگی جدید</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active"> ویژگی جدید</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements disabled -->

                    <div class="col-sm-12">
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger col-sm-3">
                                <li>{{ $error }}</li>
                            </div>
                        @endforeach
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">مشخصات ویژگی جدید</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="POST" action="{{ route('dashboard.productAttr.store') }}" >
                                @csrf
                                <div class="row">
                                    <!-- status -->
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>انتخاب محصول</label>
                                            <select class="form-control" name="product_id">
                                                @foreach($allProdcuts as $product)
                                                    <option value="{{ $product->id }}">{{ $product->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- key -->
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>نام ویژگی</label>
                                            <input disabled type="text" class="form-control" name="key" value="{{ $PrAtt->key }}" placeholder="نام ویژگی">
                                        </div>
                                    </div>

                                    <!-- value -->
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>مقدار ویژگی</label>
                                            <input type="text" class="form-control" name="value" value="{{ $PrAtt->value }}" placeholder="مقدار ویژگی">
                                        </div>
                                    </div>
                                    <!-- Submit Button -->
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="ذخیره">
                                            <a class="btn btn-danger" href="{{ route('dashboard') }}">لغو عملیات</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop

@section('footerScripts')


    <script !src="">
        $('.nav-link').removeClass('active');

        $('#categories').addClass('menu-open');
        $('#categories > a').addClass('active');
        $('#new-category').addClass('active');
    </script>
@stop
