@extends('adminpanel.layout')

@section('pageTitle')
    افزودن نوشته جدید
@stop

@section('headerStyles')
    <link href="{{ url('adminPanel/plugins/select2/select2.min.css') }}" rel="stylesheet" />
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">افزودن نوشته جدید</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">نوشته جدید</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements disabled -->

                    <div class="col-sm-12">
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger col-sm-3">
                                <li>{{ $error }}</li>
                            </div>
                        @endforeach
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">مشخصات نوشته جدید</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="POST" action="{{ route('dashboard.blog.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <!-- product -->
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <label>عنوان نوشته</label>
                                            <input type="text" class="form-control" name="topic" value="{{ old('topic') }}" placeholder="نام نوشته">
                                        </div>
                                    </div>
                                    <!-- tags -->
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <label>تگ های نوشته</label>
                                            <input type="text" class="form-control" name="tags" value="{{ old('tags') }}" placeholder="تگ های مصحولات">
                                        </div>
                                    </div>
                                    <!-- category -->
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <label>دسته بندی نوشته</label>
                                            <select class="form-control js-example-basic-single" name="category">
                                                <option value="0">دسته بندی نشده ها</option>
                                                @foreach($allCategories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->topic }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- status -->
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <label>وضعیت نوشته</label>
                                            <select class="form-control" name="category">
                                                <option value="0">انتشار</option>
                                                <option value="">پیش نویس</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Image -->
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <label for="image">عکس محصول</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="image" class="custom-file-input">
                                                    <label class="custom-file-label" for="image">انتخاب</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- description -->
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>توضیحات</label>
                                            <textarea class="form-control" id="description" name="description" rows="10" placeholder="توضیحات نوشته را در این قسمت وارد کنید">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="ذخیره">
                                            <a class="btn btn-danger" href="{{ route('dashboard.blog.index') }}">لغو عملیات</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop

@section('footerScripts')
    <!-- bs-custom-file-input -->
    <script src="{{ url('adminPanel/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/ckeditor/ckeditor.js') }}"></script>

    <script !src="">
        window.onload = function() {
            CKEDITOR.replace('description', {
                // filebrowserUploadMethod : 'form',
                filebrowserUploadUrl: '/dashboard/save_image',
                filebrowserImageUploadUrl: '/dashboard/save_image'
            });
        }
    </script>

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#blogs').addClass('menu-open');
        $('#blogs > a').addClass('active');
        $('#newBlog').addClass('active');

    </script>

    <script src="{{ url('adminPanel/plugins/select2/select2.min.js') }}"></script>

    <script !src="">
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>
@stop
