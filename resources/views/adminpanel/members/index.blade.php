@extends('adminpanel.layout')

@section('pageTitle')
    دسته بندی محصولات
@stop

@section('mainContent')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">همه ویژگی محصولات</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">داشبورد</a></li>
                        <li class="breadcrumb-item active">ویژگی محصولات</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @if(session('message'))
                        <div class="alert alert-success col-sm-3">
                            <li>{{ Session::get('message') }}</li>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-success col-sm-3">
                            <li>{{ Session::get('error') }}</li>
                        </div>
                    @endif
                </div>
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title text-right">جدول ویژگی ها</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(\App\User::all()->count() > 0)
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام کاربر</th>
                                        <th>شماره همراه</th>
                                        <th>سطح دسترسی</th>
                                        <th>تاریخ عضویت</th>
                                        <th>آخرین کد تایید</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allMembers as $member)
                                        <tr>
                                            <td>{{ $member->id }}</td>
                                            <td>{{ $member->name }}</td>
                                            <td>{{ $member->mobile }}</td>
                                            <td>{{ $member->role }}</td>
                                            <td>{{ $member->created_at }}</td>
                                            <td>{{ $member->verify_code }}</td>
                                            <td>
                                                <a href="#" style="color: gray"><i class="fas fa-pencil-alt"></i></a>
                                                <a href="#" style="color: red"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                            @else
                                <h4>ویژگی محصولی در سایت موجود نیست</h4>
                                <a class="btn btn-outline-success" href="#">اولین ویژگی خود را بساز</a>
                            @endif
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop


@section('footerScripts')

    <!-- DataTables -->
    <script src="{{ url('adminPanel/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('adminPanel/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#categories').addClass('menu-open');
        $('#categories > a').addClass('active');
        $('#allCategories').addClass('active');

    </script>
@stop

