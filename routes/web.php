<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'site\homepageController@index')->name('home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/homePage' , 'site\homepageController@index')->name('home');

Route::get('/login/sendcode' , 'Auth\LoginController@sendCode')->name('login_send_code');

//Route::group(['middleware' => ['auth' , 'admin'] ],function() {
    Route::get('/dashboard' , 'adminpanel\dashboardController@index')->name('dashboard');

    Route::group(['prefix' => '/dashboard' , 'namespace' => 'adminpanel'] , function (){

        Route::group(['prefix' => '/products' ] , function () {
            Route::get('/' , 'productController@index')->name('dashboard.product.index');
            Route::get('/create' , 'productController@create')->name('dashboard.product.create');
            Route::post('/store' , 'productController@store')->name('dashboard.product.store');
            Route::get('/edit/{id}' , 'productController@edit')->name('dashboard.product.edit');
            Route::post('/update/{id}' , 'productController@update')->name('dashboard.product.update');
            Route::get('/destroy/{id}' , 'productController@destroy')->name('dashboard.product.destroy');
        });

        Route::group(['prefix' => '/productCategory'] , function () {
            Route::get('/' , 'productCategoryController@index')->name('dashboard.productCategory.index');
            Route::get('/create' , 'productCategoryController@create')->name('dashboard.productCategory.create');
            Route::post('/store' , 'productCategoryController@store')->name('dashboard.productCategory.store');
            Route::get('/edit/{id}' , 'productCategoryController@edit')->name('dashboard.productCategory.edit');
            Route::post('/update/{id}' , 'productCategoryController@update')->name('dashboard.productCategory.update');
            Route::get('/destroy/{id}' , 'productCategoryController@destroy')->name('dashboard.productCategory.destroy');
        });

        Route::group(['prefix' => '/productAttr'] , function () {
            Route::get('/' , 'ProductAttribiuesController@index')->name('dashboard.productAttr.index');
            Route::get('/create' , 'ProductAttribiuesController@create')->name('dashboard.productAttr.create');
            Route::post('/store' , 'ProductAttribiuesController@store')->name('dashboard.productAttr.store');
            Route::get('/edit/{id}' , 'ProductAttribiuesController@edit')->name('dashboard.productAttr.edit');
            Route::post('/update/{id}' , 'ProductAttribiuesController@update')->name('dashboard.productAttr.update');
            Route::get('/destroy/{id}' , 'ProductAttribiuesController@destroy')->name('dashboard.productAttr.destroy');
        });
        Route::post('/save_image','productController@uploadImage');

        Route::group(['prefix' => '/members'] , function () {
            Route::get('/' , 'MembersController@index')->name('dashboard.members.index');
            Route::get('/create' , 'MembersController@create')->name('dashboard.members.create');
            Route::post('/store' , 'MembersController@store')->name('dashboard.members.store');
            Route::get('/edit/{id}' , 'MembersController@edit')->name('dashboard.members.edit');
            Route::post('/update/{id}' , 'MembersController@update')->name('dashboard.members.update');
            Route::get('/destroy/{id}' , 'MembersController@destroy')->name('dashboard.members.destroy');
        });

        Route::group(['prefix' => '/blog'] , function () {
            Route::get('/' , 'blogController@index')->name('dashboard.blog.index');
            Route::get('/create' , 'blogController@create')->name('dashboard.blog.create');
            Route::post('/store' , 'blogController@store')->name('dashboard.blog.store');
            Route::get('/edit/{id}' , 'blogController@edit')->name('dashboard.blog.edit');
            Route::post('/update/{id}' , 'blogController@update')->name('dashboard.blog.update');
            Route::get('/destroy/{id}' , 'blogController@destroy')->name('dashboard.blog.destroy');
        });

    });

//});

Route::group(['prefix' => '/account' , 'namespace' => 'userpanel'] , function (){
    Route::get('/' , 'dashboardController@index')->name('user.account.index');
});

Route::group(['namespace' => 'site'] , function (){
    Route::get('/product/{id}', 'ProductController@show')->name('site.product.index');

    Route::get('/productCategory/{id}', 'ProductCategoryController@show')->name('site.productCategory.index');

    Route::get('/cart', 'CartController@index')->name('site.cart.index');
    Route::post('/cart', 'CartController@store')->name('site.cart.store');
    Route::post('/update/i', 'CartController@update')->name('site.cart.update.i');
    Route::post('/update/d', 'CartController@update2')->name('site.cart.update.d');
    Route::delete('/cart/{product}', 'CartController@destroy')->name('site.cart.destroy');

    Route::get('/shop' , 'ShopController@index')->name('site.shop.index');

    Route::get('/{category}/{id}' , 'BlogController@show')->name('site.blog.single.index');
//    Route::get('/{category}' , 'BlogController@show')->name('site.blog.category.index');
});
Route::get('/logoutUser' , 'Auth\LoginController@logout')->name('logoutUser');
